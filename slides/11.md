## [Prettier](https://prettier.io/) to the rescue!

What is it? From authors page:

- an opinionated code formatter
- supports many languages
- integrates with most editors
- has few options
